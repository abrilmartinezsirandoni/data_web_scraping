import requests
from bs4 import BeautifulSoup 
import smtplib
from decouple import config

url=config('URL')
headers= {"User-Agent": config('HEADERS')}
price_value=17
email_address=config('EMAIL_ADDRESS')
password=config('PASSWORD')
receiver_email=config('RECEIVER_EMAIL') 

def trackPrices():
    price= float(getPrice())
    if price > price_value:
        diff= int(price - price_value)
        print(f"Still {diff} too expensive")
    else:
        print("cheaper! notifying")
        sendEmail()
        
def getPrice():
    page= requests.get(url,headers=headers)
    soup= BeautifulSoup(page.content, "html.parser")
    title= soup.find("h1", {"class":"ui-pdp-title"}).text.strip()
    #title= soup.find(id='productTitle').get_text().strip()
    price= soup.find("span", {"class":"andes-money-amount__fraction"}).text.strip()
    #price= soup.find(id='productTitle').get_text().strip()[1:4]
    print(title)
    print(price)
    return price

def sendEmail():
    subject="Rebajas en Mercado Libre"
    mailtext= "Subject:"+subject+"\n\n"+url
    server=smtplib.SMTP(host='smtp.gmail.com', port=587)
    server.ehlo()
    server.starttls()
    server.login(email_address, password)
    server.sendmail(email_address, receiver_email, mailtext)
    
if __name__=="__main__":
    trackPrices()
